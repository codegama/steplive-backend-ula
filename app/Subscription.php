<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{ 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'plan', 'amount', 'no_of_minutes', 'no_of_users'];

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['subscription_id', 'subscription_unique_id', 'plan_formatted', 'amount_formatted', 'currency', 'no_of_users_formatted', 'no_of_minutes_formatted'];

    public function getSubscriptionIdAttribute() {

        return $this->id;
    }

    public function getSubscriptionUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getPlanFormattedAttribute() {

        return formatted_plan($this->plan, $this->plan_type);
    }

    public function getAmountFormattedAttribute() {

        return formatted_amount($this->amount);
    }

    public function getNoOfUsersFormattedAttribute() {

        return no_of_users_formatted($this->no_of_users);
    }

    public function getNoOfMinutesFormattedAttribute() {

        return no_of_minutes_formatted($this->no_of_minutes);
    }

    public function getCurrencyAttribute() {

        return \Setting::get('currency' , '$');
    }

    public function subscriptionPayments() {

    	return $this->hasMany(SubscriptionPayment::class,'subscription_id');
    }

    /**
     * Scope a query to only include approved records.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        return $query->where('subscriptions.status', APPROVED);
    
    }

    /**
     * Scope a query to only include approved records.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCurrentPlan($query, $user_id) {

        return $query->leftJoin('subscription_payments', 'subscription_payments.subscription_id', '=', 'subscriptions.id')
            ->where('subscription_payments.user_id', $user_id)
            ->where('subscription_payments.is_current_subscription', YES)
            ->select('subscriptions.id as subscription_id', 'subscriptions.*');
    
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "SI-"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "SI-"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::deleting(function ($model){

           $model->subscriptionPayments()->delete();

        });
    }
}
