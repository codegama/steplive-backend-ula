<!DOCTYPE html>
<html lang="en">
    
<head>
    <meta charset="utf-8" />
    <title>{{Setting::get('site_name')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="{{Setting::get('site_name')}}" name="description" />
    <meta content="{{Setting::get('site_name')}}" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{Setting::get('site_icon')}}">

    <!-- plugins -->
    <link href="{{asset('admin-assets/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="{{asset('admin-assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/custom.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/assets/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/assets/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/assets/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/assets/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" /> 

     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="{{asset('admin-assets/assets/css/select2.min.css')}}">

    <link rel="stylesheet" href="{{asset('admin-assets/assets/daterangepicker/daterangepicker.css')}}">
    

    @yield('styles')

    <?php echo Setting::get('header_scripts'); ?>

</head>

<body>
    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        @include('layouts.admin.header')
        <!-- end Topbar -->

        <!-- ========== Left Sidebar Start ========== -->
        @include('layouts.admin.sidebar')
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">

            <div class="content">

                <div class="container-fluid">

                    <div class="row page-title">

                        <div class="col-md-12">

                            <nav aria-label="breadcrumb" class="float-right mt-1">

                                <ol class="breadcrumb">

                                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{tr('home')}}</a></li>

                                    @yield('bread-crumb')

                                </ol>

                            </nav>

                            <h3 class="mb-1 mt-0 text-uppercase">@yield('content-header')</h3>

                        </div>

                    </div>

                </div>

                @include('notifications.notify')

                @yield('content') 
            
                <!-- Footer Start -->
                @include('layouts.admin.footer')
                <!-- end Footer -->
            </div>

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

    </div>
    <!-- END wrapper -->

    @include('layouts.admin._logout_model')

    @include('layouts.admin.scripts')

    @yield('scripts')

    <?php echo Setting::get('google_analytics'); ?>

    <?php echo Setting::get('body_scripts'); ?>


</body>

</html>